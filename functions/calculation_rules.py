"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""

from functions.classes import *

def test_function():
    """Test function to check module functionality"""
    print("You called the test function.")

def kpi_mass(system: LegoAssembly)->float:
    """
    Calculates the total mass of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_mass (float): Sum of masses of all components in the system in g

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_mass()")

    total_mass = 0
    for c in system.get_component_list(-1):
        total_mass += c.properties["mass [g]"]
    return total_mass # alternative: sum(c.properties["mass [g]"] for c in system.get_component_list(-1))

# Add new functions for calculating metrics

def kpi_cost(system: LegoAssembly)->float:
    """
    Calculates the total costs of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_cost (float): Sum of costs of all components in the system in Euro

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_cost()")

    total_cost = 0
    for c in system.get_component_list(-1):
        total_cost += c.properties["price [Euro]"]
    total_cost = round(total_cost, 2)    
    return total_cost 

def kpi_time(system: LegoAssembly)->float:
    """
    Determines the longest delivery time of the system components

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        delivery_time (float): The longest delivery time of all components in the system

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_time()")

    delivery_time = 0
    for c in system.get_component_list(-1):
        if delivery_time < c.properties["delivery time [days]"]:
            delivery_time = c.properties["delivery time [days]"]
    return delivery_time 


if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )
